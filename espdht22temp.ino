#include <ESP8266WiFi.h>
//#include <WifiClient.h>
#include <DHT.h>

#define DHTPIN 2 
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

const char WiFiSSID[] = "";
const char WiFiPSK[] = "";
String deviceName = "ESP-07:01";

const int LED_PIN = 0; 
const unsigned long postRate = 60000 * 15; //check in once per 15 minutes
unsigned long lastPost = 0;
const char herokuHost[] = "whispering-brook-51660.herokuapp.com";

void connectWiFi(){
  byte ledStatus = LOW;

  // Set WiFi mode to station (as opposed to AP or AP_STA)
  WiFi.mode(WIFI_STA);

  WiFi.begin(WiFiSSID, WiFiPSK);

  while (WiFi.status() != WL_CONNECTED){
    // Blink the LED
    digitalWrite(LED_PIN, ledStatus); // Write LED high/low
    ledStatus = (ledStatus == HIGH) ? LOW : HIGH;

    delay(100);//delay for TCP stack maintenance and LED modulation
    }
}

void initHardware(){
  //Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  dht.begin();
}

void setup() {
  initHardware();
  connectWiFi();
  digitalWrite(LED_PIN, HIGH);
}

void sendRequest(float celsiusReading, float fahrenheitReading, float humidityReading, WiFiClient client){
  //send http get request
  client.print("GET /logService/?deviceName=");
  client.print(deviceName);
  client.print("&reportC=");
  client.print(celsiusReading);
  client.print("&reportF=");
  client.print(fahrenheitReading);
  client.print("&status=");
  client.print("Check-in%20OK.%20Reading:%20"); client.print(celsiusReading); client.print("%20C%20"); client.print(fahrenheitReading); client.print("%20F%20"); client.print(humidityReading); client.print("%20humidity");
  client.println(" HTTP/1.1");
  client.print("Host: ");
  client.println(herokuHost);
  client.println("Connection: close");
  client.println();

  while(client.available()){
    String line = client.readStringUntil('\r');
  }
}

int postToHeroku(){
  // LED turns on when we enter, it'll go off when we 
  // successfully post.
  float celsiusReading,fahrenheitReading, humidityReading;
  digitalWrite(LED_PIN, HIGH);

  // Do a little work to get a unique-ish name. Append the
  // last two bytes of the MAC (HEX'd) to "Thing-":
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  WiFi.macAddress(mac);
  //String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
  //               String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
  //macID.toUpperCase();

  WiFiClient client;
  const int httpPort = 80;
  
  if (!client.connect(herokuHost, httpPort)) {
    // If we fail to connect, return 0.
    return 0;
  }

  celsiusReading = dht.readTemperature();
  humidityReading = dht.readHumidity();
  fahrenheitReading = (celsiusReading * (9.0 / 5.0)) + 32;
  sendRequest(celsiusReading, fahrenheitReading, humidityReading, client);

  digitalWrite(LED_PIN, LOW);

  return 1; // Return success
}


void loop() {
  //create a post right at the start for debugging and verification purposes
  if (lastPost + postRate <= millis()){
    if (postToHeroku())
      lastPost = millis();
    else
      delay(1000);    
  }
}

